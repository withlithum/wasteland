﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rage.Native;

namespace TheWasteLand.Engine.Native
{
    /// <summary>Defines native functions so they can be called easily.</summary>
    /// <seealso cref="NativeFunction" />
    public static class Api
    {
        /// <summary>Sets is the riot mode enabled.</summary>
        /// <param name="enabled">if set to <c>true</c>, enables the riot mode.</param>
        public static void SetRiotModeEnabled(bool enabled)
        {
            NativeFunction.Natives.x2587A48BC88DFADF(enabled);
        }
    }
}
