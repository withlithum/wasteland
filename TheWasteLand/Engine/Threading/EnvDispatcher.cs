﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheWasteLand.Engine.Threading
{
    internal static class EnvDispatcher
    {
        private static readonly List<Action> dispatching = new List<Action>();

        internal static void Tick()
        {
            foreach (var action in dispatching)
            {
                action?.Invoke();
            }
        }
    }
}
