﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Rage.Attributes;
using TheWasteLand.Engine;

[assembly: Plugin("The Waste Land", Author = "RelaperCrystal", PrefersSingleInstance = true, EntryPoint = "TheWasteLand.EntryPoint.Main", Description = "Sort of survival mod")]

namespace TheWasteLand
{
    internal static class EntryPoint
    {
        /// <summary>Defines the entry point of the application.</summary>
        public static void Main()
        {
            GameEnvironment.Initialize();
        }
    }
}
